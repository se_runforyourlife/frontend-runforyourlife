import 'whatwg-fetch';
import { RFYLResponse } from '../types';
import { apiEndpoint } from '../config';

class FetchWrapper {
  private readonly baseRequestInit: RequestInit = {
    mode: 'cors',
    cache: 'no-cache',
  };

  public constructor(private baseUrl: string) {}

  public async get<T>(path: string, data?: object, headers?: HeadersInit): Promise<RFYLResponse<T>> {
    const url = new URL(this.baseUrl + path);
    const requestInit: RequestInit = {
      method: 'GET',
      ...this.baseRequestInit,
    };
    if (data) {
      Object.entries(data).forEach(([key, value]) => url.searchParams.append(key, value));
    }
    return await this.request(String(url), requestInit, headers);
  }

  public async post<T>(path: string, data?: object, headers?: HeadersInit): Promise<RFYLResponse<T>> {
    const requestInit: RequestInit = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
      ...this.baseRequestInit,
    };
    return await this.request(this.baseUrl + path, requestInit, headers);
  }

  public async put<T>(path: string, data?: object, headers?: HeadersInit): Promise<RFYLResponse<T>> {
    const requestInit: RequestInit = {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
      ...this.baseRequestInit,
    };
    return await this.request(this.baseUrl + path, requestInit, headers);
  }

  public async patch<T>(path: string, data?: object, headers?: HeadersInit): Promise<RFYLResponse<T>> {
    const requestInit: RequestInit = {
      method: 'PATCH',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
      ...this.baseRequestInit,
    };
    return await this.request(this.baseUrl + path, requestInit, headers);
  }

  public async delete<T>(path: string, data?: object, headers?: HeadersInit): Promise<RFYLResponse<T>> {
    const url = new URL(this.baseUrl + path);
    const requestInit: RequestInit = {
      method: 'DELETE',
      ...this.baseRequestInit,
    };
    if (data) {
      Object.entries(data).forEach(([key, value]) => url.searchParams.append(key, value));
    }
    return await this.request(String(url), requestInit, headers);
  }

  public async upload<T>(path: string, key: string, file: File, headers?: HeadersInit): Promise<RFYLResponse<T>> {
    const data = new FormData();
    data.append(key, file);
    const requestInit: RequestInit = {
      method: 'POST',
      body: data,
    };
    return await this.request(this.baseUrl + path, requestInit, headers);
  }

  private async request<T>(path: string, requestInit: RequestInit, headers?: HeadersInit): Promise<RFYLResponse<T>> {
    if (headers) {
      headers = {
        ...requestInit.headers,
        ...headers,
      };
      requestInit.headers = headers;
    }
    const response = await fetch(path, requestInit);
    console.log(response);
    return {
      status: response.status,
      data: await response.json(),
    };
  }
}

export default new FetchWrapper(apiEndpoint);
