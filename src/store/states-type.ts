import SessionModule from './session-module';
import UserModule from './user-module';

export interface RootState {
  SessionModule: SessionModule;
  UserModule: UserModule;
}
