import apiService from './api-service';
import { SearchData, PreFilter } from '@/types';
import { Event } from '@/types/event';


class SearchService {
  public  async searchEvent(data: SearchData) {
    return  await apiService.post<Event[]>('/search/results', data);
  }

  public async getFilter() {
    return  await apiService.get<PreFilter[]>('/search/typeEvents');
  }
}

export default new SearchService();
