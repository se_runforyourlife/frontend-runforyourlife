export interface PaymentDetail {
  date: Date,
  TransactionId : string,
  EventName: string,
  RunnerName: string,
  Amount: number,
  Type : 'Registration Fee' | 'Refund',
}

export interface PaymentLists {
  data: PaymentDetail[]
}

