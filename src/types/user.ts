export enum Role {
  EventManager = 'EventManager',
  Runner = 'Runner',
  Admin = 'Admin',
}

export interface User {
  id?: string;
  role?: Role;
}

export interface Credential {
  id: string;
  password: string;
}

export interface UserData {
  bankAccount: null | string;
  bankAccountNumber: null | string;
  birthDate: string;
  firstname: string;
  gender: string;
  id: string;
  idCardNumber: string;
  lastname: string;
  password: string | null;
  phoneNumber: string;
  role: string;
  imageURL?: string;
}

export interface UpdateDataForm {
  type: 'bankAccount' | 'bankAccountNumber' | 'birthDate' | 'firstname' | 'gender' | 'id' | 'idCardNumber' | 'lastname' | 'password' | 'phoneNumber' | 'role' ;
  value: string;
}
