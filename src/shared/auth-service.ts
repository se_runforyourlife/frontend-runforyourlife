import fetchWrapper from './fetch-wrapper';
import { RFYLResponse, Credential, AuthTokens, Message, HTTP_STATUS_UNAUTHORIZED } from '@/types';

class AuthService {
  public async login(credential: Credential): Promise<RFYLResponse<AuthTokens | Message | undefined>> {
    return await fetchWrapper.post<AuthTokens>('/auth/login', credential);
  }

  public async refresh(): Promise<RFYLResponse<AuthTokens | Message | undefined>> {
    return Promise.resolve({
      status: HTTP_STATUS_UNAUTHORIZED,
    });
  }
}

export default new AuthService();
