# frontend-runforyourlife

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Run with docker
```
docker build -t "runforyourlife-web:latest" .

docker container run -p 80:80 runforyourlife-web
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
