import apiService from './api-service';
import { RegistrationTransaction, HTTP_STATUS_OK, Message } from '@/types';
import store from '@/store/store';

class ReservationService {
  public async getUserTransaction(): Promise<RegistrationTransaction[]> {
    const runnerId = store.getters.currentUser.id;
    const response = await apiService.get<RegistrationTransaction[]>('/transaction', { runnerId }, true);
    if (response.data === undefined) {
      throw new Error('No reservation data from server recieved.');
    }
    if (response.status !== HTTP_STATUS_OK) {
      throw new Error((response.data as Message).message);
    }
    return response.data;
  }
}

export default new ReservationService();
