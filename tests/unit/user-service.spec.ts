import userService from '@/shared/user-service';
import fetchMock from 'fetch-mock';
import { apiEndpoint } from '@/config';

describe('testing user service', () => {
  it('should post to the runner api', async () => {
    fetchMock.postOnce(apiEndpoint + '/user/runner', {});
    const response = userService.createRunner({
      id: 'id',
      password: 'password',
      firstname: 'first',
      lastname: 'last',
      idCardNumber: '1234567890123',
      phoneNumber: '0888888888',
      birthDate: '1/1/1970',
      gender: 'm',
    });
    expect(response).resolves.not.toThrowError();
  });

  it('should post to the event manager api', async () => {
    fetchMock.postOnce(apiEndpoint + '/user/event-manager', {});
    const response = userService.createEventManager({
      id: 'id',
      password: 'password',
      firstname: 'first',
      lastname: 'last',
      idCardNumber: '1234567890123',
      phoneNumber: '0888888888',
      birthDate: '1/1/1970',
      gender: 'm',
      bankAccount: 'bank',
      bankAccountNumber: '123456789',
    });
    expect(response).resolves.not.toThrowError();
  });
});
