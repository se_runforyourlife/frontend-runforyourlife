import apiService from './api-service';
import { SearchData, PreFilter, EventManageForm, RFYLResponse, Message, User } from '@/types';
import { Event, CreatedEvent } from '@/types/event';

class EventService {
  public async getEventByID(id: string | string[]) {
    return await apiService.get<Event>(`/event/${id}`);
  }
  public async getOwner(id: string) {
    return await apiService.get<{ owner: User }>(`/event/${id}/owner`);
  }
  public async getReservedEventByID(runnerId: string | string[], eventId: string | string[]) {
    return await apiService.get<any>(`/transaction/?runnerId=${runnerId}&eventId=${eventId}`);
  }
  public async getReservedEventByRunnerID(runnerId: string | string[]) {
    return await apiService.get<any>(`/transaction/?runnerId=${runnerId}`);
  }
  public async mockEventByID(id: string | string[]) {
    return await apiService.patch<Event>(`/event/${id}/mock`);
  }

  public async getAllEvent() {
    return await apiService.get<Event[]>('/event');
  }

  public async removeEvent(eventId: string): Promise<RFYLResponse<Message>> {
    return await apiService.delete(`/event/${eventId}`, {}, true);
  }

  public async getMyEvent(id: string) {
    return await apiService.get<Event[]>(`/event/?eventManagerId=${id}`);
  }

  public async createEvent(eventManageForm: EventManageForm): Promise<RFYLResponse<CreatedEvent>> {
    return await apiService.post<CreatedEvent>('/event', eventManageForm, true);
  }

  public async editEvent(id: string, eventManageForm: EventManageForm): Promise<RFYLResponse<Message>> {
    return await apiService.patch<Message>(`/event/${id}`, eventManageForm, true);
  }

  public async deleteEvent(id: string): Promise<RFYLResponse<Message>> {
    return await apiService.delete<Message>(`/event/${id}`, {}, true);
  }

  public async uploadImage(file: File, eventId: string): Promise<RFYLResponse<Message>> {
    return await apiService.upload(`/upload/image/${eventId}`, 'image', file, true);
  }

  public async setEventStatus(id: string, isRegister: boolean): Promise<RFYLResponse<Message>> {
    return await apiService.patch<Message>(`/event/${id}/status`, { isRegister }, true);
  }
  public async reserveEvent(
    eventId: string,
    method: string,
    feeId: string,
    shirtSize?: string): Promise<RFYLResponse<Message>> {
    return await apiService.post(`/transaction/reserve/${eventId}`, { method, feeId, shirtSize }, true);
  }

  public async payEvent(eventId: string): Promise<RFYLResponse<Message>> {
    return await apiService.post(`/transaction/pay/${eventId}`, {}, true);
  }
  public async getPayment(role: string, userId: string): Promise<RFYLResponse<any>> {
    return await apiService.get(`/transaction/?${role}=${userId}`);
  }
  public async getTransactionByUserId(userId: string): Promise<RFYLResponse<any>> {
    return await apiService.get(`/transaction`, { userId });
  }

  public async cancelReservation(eventId: number): Promise<RFYLResponse<any>> {
    return await apiService.post(`/transaction/cancel/${eventId}`, {}, true);
  }
  public async getTransactionById(id: string | string[]) {
    return await apiService.get<any[]>(`/transaction/?eventId=${id}`);
  }
  public async removeParticipant(eventID: string, runnerID: string): Promise<RFYLResponse<Message>> {
    return await apiService.post(`/transaction/disjoin/${eventID}/${runnerID}`, {}, true);
  }

}

export default new EventService();
