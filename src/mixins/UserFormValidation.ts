import Vue from 'vue';
import { Mixin } from 'vue-mixin-decorator';

@Mixin
export default class UserFormValidation extends Vue {
  public validationRule = {
    id: { required: true, alpha_dash: true },
    password: { required: true, regex: /^\S*$/ },
    confirmPassword: { required: true, confirmed: 'password' },
    firstname: { required: true },
    lastname: { required: true },
    idCardNumber: { required: true, digits: 13 },
    phoneNumber: { required: true, regex: /^[0-9]*$/ },
    birthDate: { required: true },
    gender: { required: true },
    bankAccount: { required: true },
    bankAccountNumber: { required: true, regex: /^[0-9]*$/ },
  };

  public validationDict = {
    custom: {
      'id': {
        alpha_dash: 'The id field may contains A-Z, a-z, 0-9, - and _.',
      },
      'password': {
        regex: 'The password field cannot contain spaces.',
      },
      'phone number': {
        regex: 'This phone number can only contain digits.',
      },
      'bankAccountNumber': {
        regex: 'The bank account number field can only contain digits.',
      },
      'confirmPassword': {
        confirmed: 'The password confirmation does not match.',
      },
    },
    attributes: {
      IDCard: 'ID card',
      BirthDate: 'birthdate',
      bankAccount: 'bank account',
      bankAccountNumber: 'bank account number',
      confirmPassword: 'confirm password',
    },
  };

  public created() {
    this.$validator.localize('en', this.validationDict);
  }

  public get isSubmitDisabled(): boolean {
    return this.errors.items.length > 0;
  }
}
