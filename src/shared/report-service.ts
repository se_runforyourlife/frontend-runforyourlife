import apiService from './api-service';
import { RFYLResponse, Message, Report, CreateReportForm } from '@/types';

class ReportService {
  public async getAllReport(): Promise<RFYLResponse<Report[]>> {
    return apiService.get<Report[]>('/report');
  }

  public async createReport(data: CreateReportForm): Promise<RFYLResponse<Message>> {
    return apiService.post<Message>('/report', data, true);
  }
}

export default new ReportService();
