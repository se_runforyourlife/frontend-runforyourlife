import jwtDecode from 'jwt-decode';
import { AuthTokens, User } from '@/types';

class SessionService {
  public saveToken({ token, refreshToken }: AuthTokens): void {
    localStorage.setItem('token', token);
    localStorage.setItem('refreshToken', refreshToken);
  }

  public destroyToken(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
  }

  public getToken(): AuthTokens {
    return {
      token: localStorage.getItem('token') || '',
      refreshToken: localStorage.getItem('refreshToken') || '',
    };
  }

  public getDecodedUser(): User {
    const authTokens = this.getToken();
    if (authTokens.token) {
      return jwtDecode(this.getToken().token) as User;
    }
    return {};
  }
}

export default new SessionService();
