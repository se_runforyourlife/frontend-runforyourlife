import store from '@/store/store';

import { NavigationGuard } from 'vue-router';

export const authMiddleware: NavigationGuard = (to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!store.state.SessionModule.isAuthenticated) {
      next({
        path: '/login',
        query: { redirect: to.fullPath },
      });
    }
  }
  next();
};
