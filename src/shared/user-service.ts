import apiService from './api-service';
import {
  RunnerRegistrationForm,
  Message,
  EventManagerRegistrationForm,
  HTTP_STATUS_OK,
  HTTP_STATUS_BAD_REQUEST,
  User,
  UserData,
} from '@/types';

class UserService {
  public async createRunner(data: RunnerRegistrationForm) {
    const response = await apiService.post('/user/runner', data);
    if (response.status !== HTTP_STATUS_OK) {
      if (response.status === HTTP_STATUS_BAD_REQUEST && response.data) {
        const error = response.data as Message;
        throw new Error(error.message);
      }
      throw new Error();
    }
  }

  public async createEventManager(data: EventManagerRegistrationForm) {
    const response = await apiService.post('/user/event-manager', data);
    if (response.status !== HTTP_STATUS_OK) {
      if (response.status === HTTP_STATUS_BAD_REQUEST  && response.data) {
        const error = response.data as Message;
        throw new Error(error.message);
      }
      throw new Error();
    }
  }

  public async updateRunner(data: UserData) {
    const response = await apiService.post('/user/update-runner', data, true);
    if (response.status !== HTTP_STATUS_OK) {
      if (response.status === HTTP_STATUS_BAD_REQUEST  && response.data) {
        const error = response.data as Message;
        throw new Error(error.message);
      }
      throw new Error();
    }
    return response;
  }

  public async getUserData(data: any) {
    const response = await apiService.post<UserData>('/user/userid', data);
    if (response.status !== HTTP_STATUS_OK) {
      if (response.status === HTTP_STATUS_BAD_REQUEST  && response.data) {
        const error = response.data as Message;
        throw new Error(error.message);
      }
      throw new Error();
    }
    return response;
  }

  public async uploadProfileImage(file: File, id: string) {
    return await apiService.upload(`/upload/image/profile/${id}`, 'image', file, true);
  }
}

export default new UserService();
