import { RegistrationFee } from './event';

export interface UserRegistrationForm {
  id: string;
  password: string;
  firstname: string;
  lastname: string;
  idCardNumber: string;
  phoneNumber: string;
  birthDate: string;
  gender: string;
}

export interface EventManagerRegistrationForm extends UserRegistrationForm {
  bankAccount: string;
  bankAccountNumber: string;
}

export type RunnerRegistrationForm = UserRegistrationForm;

export interface EventManageForm {
  name: string;
  imageUrl?: string;
  type: string;
  date: string;
  isRegister: boolean;
  location: string;
  minFee: number;
  maxFee: number;
  feeList: RegistrationFee[];
  detail?: string;
  ownerId?: string;
  id?: string;
}

export interface CreateReportForm {
  reason: string;
  reportedId: string;
  reportedEvent: number;
}
