import { User } from './user';

export interface Event {
  id: number;
  name: string;
  imageURL: string;
  type: string;
  date: Date;
  isRegister: boolean;
  location: string;
  minFee: number;
  maxFee: number;
  detail: string;
  feeList: RegistrationFee[];
}

export interface RegistrationFee {
  description: string;
  price: number;
}

export interface CreatedEvent {
  message: string;
  id: string;
}

export interface RegistrationTransaction {
  createdAt: string;
  updatedAt: string;
  isPaid: boolean;
  method: string;
  event: Event;
  runner: User;
  fee: RegistrationFee;
}
