export interface SearchData {
  search: string | string[];
  filters: string | string[];
}

export interface GroupFilter {
  language: string;
  libs: Array<{
      name: string;
      category: string;
  }>;
}

export interface Filter {
  name: string;
  category: string;
}

export interface PreFilter {
  id: number;
  name: string;
}

