import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import SearchResult from './views/SearchResult.vue';
import EventDetail from './views/EventDetail.vue';
import EditProfile from './views/EditProfile.vue';
import CreateEvent from './views/CreateEvent.vue';
import Reservation from './components/Reservation.vue';
import { authMiddleware } from './shared/auth-middleware';
import Eventlists from './views/EventLists.vue';
import AdminEventLists from './views/admin/AdminEventLists.vue';
import AdminReport from './views/admin/AdminReport.vue';
import Admin from './views/admin/Admin.vue';

import EventsCalendar from './views/EventsCalendar.vue';
import PaymentList from './views/PaymentList.vue';
import viewParticipant from './views/viewParticipant.vue';
import ReservationLists from './views/ReservationLists.vue';


Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/search',
      name: 'search',
      component: SearchResult,
    },
    {
      path: '/eventList',
      name: 'eventList',
      component: Eventlists,
    },
    {
      path: '/event',
      name: 'event',
      component: EventDetail,
    },
    {
      path: '/event/create',
      name: 'createEvent',
      component: CreateEvent,
    },
    {
      path: '/event/participant/:id',
      name: 'participant',
      component: viewParticipant ,
      props: true,
    },
    {
      path: '/event/edit/:id',
      name: 'editEvent',
      component: CreateEvent,
      props: true,
    },
    {
      path: '/profile',
      name: 'profile',
      component: EditProfile,
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin,
      children: [
        {
          path: 'eventList',
          name: 'adminEventList',
          component: AdminEventLists,
        },
        {
          path: 'report',
          name: 'adminReport',
          component: AdminReport,
        },
      ],
    }, {
      path: '/calendar',
      name: 'calendar',
      component: EventsCalendar,
      props: true,
    },
    {
      path: '/payment/list',
      name: 'paymentList',
      component: PaymentList,
    },
    {
      path: '/reservation',
      name: 'reservation',
      component: Reservation,

    },
    {
      path: '/reservationList',
      name: 'reservationList',
      component: ReservationLists,
    },
  ],
});

router.beforeEach(authMiddleware);

export default router;
