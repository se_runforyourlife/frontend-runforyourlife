import Vue from 'vue';
import Vuex from 'vuex';
import SessionModule from './session-module';
import UserModule from './user-module';
import { RootState } from './states-type';

Vue.use(Vuex);

export default new Vuex.Store<RootState>({
  state: {
    SessionModule: SessionModule.state,
    UserModule: UserModule.state,
  },
  mutations: {

  },
  actions: {

  },
  modules: {
    SessionModule,
    UserModule,
  },
});
