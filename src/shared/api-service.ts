import fetchWrapper from './fetch-wrapper';
import { RFYLResponse } from '../types';
import sessionService from './session-service';

class ApiService {

  public async get<T>(path: string, data?: object, auth = false): Promise<RFYLResponse<T>> {
    return await fetchWrapper.get(path, data, this.getHeaders(auth));
  }

  public async post<T>(path: string, data?: object, auth = false): Promise<RFYLResponse<T>> {
    return await fetchWrapper.post(path, data, this.getHeaders(auth));
  }

  public async put<T>(path: string, data?: object, auth = false): Promise<RFYLResponse<T>> {
    return await fetchWrapper.put(path, data, this.getHeaders(auth));
  }

  public async patch<T>(path: string, data?: object, auth = false): Promise<RFYLResponse<T>> {
    return await fetchWrapper.patch(path, data, this.getHeaders(auth));
  }

  public async delete<T>(path: string, data?: object, auth = false): Promise<RFYLResponse<T>> {
    return await fetchWrapper.delete(path, data, this.getHeaders(auth));
  }

  public async upload<T>(path: string, key: string, file: File, auth = false): Promise<RFYLResponse<T>> {
    return await fetchWrapper.upload(path, key, file, this.getHeaders(auth));
  }

  private getHeaders(auth: boolean): HeadersInit | undefined {
    if (auth) {
      return {
        Authorization: `Bearer ${sessionService.getToken().token}`,
      };
    }
  }
}

export default new ApiService();
