import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/store';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faSearch,
  faSpinner,
  faCalendar,
  faDollarSign,
  faSearchLocation,
  faPassport,
  faMapMarker,
  faBook,
  faCloudUploadAlt,
  faPencilAlt,
  faTrash,
  faCreditCard,
} from '@fortawesome/free-solid-svg-icons';
import VeeValidate from 'vee-validate';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import 'bootstrap/dist/css/bootstrap.css';
import 'ant-design-vue/dist/antd.css';
import VueSweetalert2 from 'vue-sweetalert2';
import { Calendar,Table,Badge } from 'ant-design-vue';
Vue.use(Calendar);
Vue.use(Table);
Vue.use(Badge)
Vue.use(VueSweetalert2);
Vue.use(VeeValidate, {
  classes: true,
  classNames: {
    invalid: 'is-invalid',
  },
});

library.add(
  faSearch,
  faSpinner,
  faCalendar,
  faDollarSign,
  faSearchLocation,
  faPassport,
  faMapMarker,
  faBook,
  faCloudUploadAlt,
  faPencilAlt,
  faTrash,
  faCreditCard
);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
