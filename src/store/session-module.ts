import { Module, VuexModule, Mutation, Action} from 'vuex-module-decorators';
import { User, AuthTokens, Credential, Message, HTTP_STATUS_OK } from '@/types';
import sessionService from '@/shared/session-service';
import { SET_ERROR, SET_AUTH, PURGE_AUTH } from './mutations-type';
import { LOGIN, LOGOUT } from './actions-type';
import authService from '@/shared/auth-service';

@Module
export default class SessionModule extends VuexModule {
  public user: User = sessionService.getDecodedUser();
  public errors: string[] = [];
  public isAuthenticated = !!sessionService.getToken().token;

  get currentUser(): User {
    return this.user;
  }

  @Mutation
  public [SET_ERROR](errors: string[]) {
    this.errors = errors;
  }

  @Mutation
  public [SET_AUTH](authTokens: AuthTokens) {
    sessionService.saveToken(authTokens);
    this.isAuthenticated = true;
    this.errors = [];
    this.user = sessionService.getDecodedUser();
  }

  @Mutation
  public [PURGE_AUTH]() {
    sessionService.destroyToken();
    this.isAuthenticated = false;
    this.errors = [];
    this.user = sessionService.getDecodedUser();
  }

  @Action
  public async [LOGIN](credential: Credential): Promise<User | undefined> {
    const response = await authService.login(credential);
    if (response.status === HTTP_STATUS_OK && response.data) {
      this.context.commit(SET_AUTH, response.data);
      return sessionService.getDecodedUser();
    } else {
      const message = response.data as Message;
      this.context.commit(SET_ERROR, [message.message || 'Authentication Error']);
      throw new Error(message.message);
    }
  }

  @Action
  public [LOGOUT](): void {
    this.context.commit(PURGE_AUTH);
  }
}
