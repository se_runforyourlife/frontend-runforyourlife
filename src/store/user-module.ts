import { Module, VuexModule, Mutation, Action} from 'vuex-module-decorators';
import { User, AuthTokens, Credential, Message, HTTP_STATUS_OK, UserData, UpdateDataForm } from '@/types';
import sessionService from '@/shared/session-service';
import { SET_ERROR, SET_AUTH, PURGE_AUTH, SET_USER } from './mutations-type';
import { LOGIN, LOGOUT, UPDATE_USER } from './actions-type';
import authService from '@/shared/auth-service';

@Module
export default class UserModule extends VuexModule {
  public userData: UserData = {
    bankAccount: null,
    bankAccountNumber: null,
    birthDate: '',
    firstname: '',
    gender: '',
    id: '',
    idCardNumber: '',
    lastname: '',
    password: '',
    phoneNumber: '',
    role: '',
  };

  get currentUserData(): UserData {
    return this.userData;
  }
  get bankAccount(): string | null {
    return this.userData.bankAccount;
  }
  get bankAccountNumber(): string | null {
    return this.userData.bankAccountNumber;
  }
  get birthDate(): string {
    return this.userData.birthDate;
  }
  get firstname(): string {
    return this.userData.firstname;
  }
  get gender(): string {
    return this.userData.gender;
  }
  get id(): string {
    return this.userData.id;
  }
  get idCardNumber(): string {
    return this.userData.idCardNumber;
  }
  get lastname(): string {
    return this.userData.lastname;
  }
  get password(): string |null {
    return this.userData.password;
  }
  get phoneNumber(): string {
    return this.userData.phoneNumber;
  }
  get role(): string {
    return this.userData. role;
  }


  @Mutation
  public [SET_USER](data: UpdateDataForm) {
    this.userData[data.type] = data.value;
  }

  @Action
  public  [UPDATE_USER](data: UpdateDataForm): void {
    this.context.commit(SET_USER, data);
  }

  @Action
  public [LOGOUT](): void {
    this.context.commit(PURGE_AUTH);
  }
}
