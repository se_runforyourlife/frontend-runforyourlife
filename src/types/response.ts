export interface RFYLResponse<T> {
  status: number;
  data?: T;
}

export interface Message {
  message?: string;
}

