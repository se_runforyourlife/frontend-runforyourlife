import fetchMock from 'fetch-mock';
import authService from '@/shared/auth-service';
import { apiEndpoint } from '@/config';
import { HTTP_STATUS_UNAUTHORIZED, HTTP_STATUS_OK, AuthTokens } from '@/types';

describe('testing auth service', () => {
  it('should return unauthorized (401)', async () => {
    const response = await authService.refresh();
    expect(response.status).toBe(HTTP_STATUS_UNAUTHORIZED);
  });

  it('should return token and refresh token', async () => {
    fetchMock.post(apiEndpoint + '/auth/login', {
      token: 'token',
      refreshToken: 'refreshToken',
    });
    const response = await authService.login({
      id: 'id',
      password: 'password',
    });
    expect(response.status).toBe(HTTP_STATUS_OK);
    expect(response.data).not.toBeFalsy();
    const authTokens = response.data as AuthTokens;
    expect(authTokens.token).toBe('token');
    expect(authTokens.refreshToken).toBe('refreshToken');
    fetchMock.reset();
  });
});

