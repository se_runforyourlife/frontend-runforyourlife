import { User } from './user';
import { Event } from './event';

export interface Report {
  id: number;
  reason: string;
  reportFrom: User;
  reportTo: User;
  reportEvent?: Event;
}
